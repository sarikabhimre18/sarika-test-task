import { useEffect, useRef, useState } from "react";
import "./styles.css";

function App() {
  const [newCountries, setNewCountries] = useState([]);
  const [countryText, setCountryText] = useState("");
  const [searchHistory, setSearchHistory] = useState([]);
  const [showCountries, setShowCountries] = useState(false);
  const dropdownRef = useRef(null);

  useEffect(() => {
    callApi();
  }, []);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setShowCountries(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [dropdownRef]);

  const callApi = () => {
    fetch("https://api.first.org/data/v1/countries", { method: "GET" })
      .then((data) => data.json())
      .then((json) => {
        const countryNames = Object.values(json.data).map(
          (item) => item.country
        );
        setNewCountries(countryNames);
      })
      .catch((error) => console.error("Error fetching countries:", error));
  };

  const filteredCountries = countryText
    ? newCountries.filter((country) =>
        country.toLowerCase().includes(countryText.toLowerCase())
      )
    : newCountries;

  const addSearchHistory = (searchedText) => {
    if (searchedText === "") return;
    const now = new Date();
    const formattedDate = now.toLocaleDateString("en-CA");
    const formattedTime = now.toLocaleTimeString("en-US", {
      hour: "numeric",
      minute: "numeric",
      hour12: true,
    });
    setSearchHistory([
      ...searchHistory,
      { searchText: searchedText, date: `${formattedDate}, ${formattedTime}` },
    ]);
    setCountryText(searchedText);
    setShowCountries(false);
  };

  const removeSearchHistory = (index) => {
    setSearchHistory(searchHistory.filter((_, i) => i !== index));
  };

  return (
    <div className="search-engine-container">
      <h1 className="search-engine-title">Country Search</h1>
      <div className="search-engine-wrapper">
        <div className="search-input-wrapper">
          <svg
            fill="none"
            height="20"
            stroke="currentColor"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="2"
            viewBox="0 0 24 24"
            width="20"
            xmlns="http://www.w3.org/2000/svg"
            className="search-icon"
          >
            <circle cx="11" cy="11" r="8" />
            <line x1="21" x2="16.65" y1="21" y2="16.65" />
          </svg>
          <input
            type="text"
            name="countryText"
            onChange={(event) => setCountryText(event.target.value)}
            value={countryText}
            onFocus={() => {
              setShowCountries(true);
            }}
            className="search-input"
            placeholder="Search here...."
          />
          <div className="close-btn" onClick={() => setCountryText("")}>
            {countryText && (
              <svg
                version="1.1"
                id="Layer_1"
                x="0px"
                y="0px"
                width="12"
                height="12"
                viewBox="0 0 122.878 122.88"
                enable-background="new 0 0 122.878 122.88"
              >
                <g>
                  <path d="M1.426,8.313c-1.901-1.901-1.901-4.984,0-6.886c1.901-1.902,4.984-1.902,6.886,0l53.127,53.127l53.127-53.127 c1.901-1.902,4.984-1.902,6.887,0c1.901,1.901,1.901,4.985,0,6.886L68.324,61.439l53.128,53.128c1.901,1.901,1.901,4.984,0,6.886 c-1.902,1.902-4.985,1.902-6.887,0L61.438,68.326L8.312,121.453c-1.901,1.902-4.984,1.902-6.886,0 c-1.901-1.901-1.901-4.984,0-6.886l53.127-53.128L1.426,8.313L1.426,8.313z" />
                </g>
              </svg>
            )}
          </div>
        </div>
        {showCountries && filteredCountries.length > 0 && (
          <div className="search-result-wrap" ref={dropdownRef}>
            {filteredCountries.map((country, index) => (
              <div
                key={index}
                dangerouslySetInnerHTML={{
                  __html: country.replace(
                    new RegExp(`(${countryText})`, "gi"),
                    (match) => `<b>${match}</b>`
                  ),
                }}
                onClick={() => {
                  addSearchHistory(country);
                }}
                className="search-item"
              />
            ))}
          </div>
        )}
        {filteredCountries.length <= 0 && (
          <div className="no-result-wrap">No result found</div>
        )}
      </div>

      {searchHistory.length > 0 && (
        <div className="search-history-wrap">
          <div className="search-history-header">
            <h3 className="search-history-title">Search History</h3>
            <span
              onClick={() => setSearchHistory([])}
              className="clear-history-link"
            >
              Clear all history
            </span>
          </div>
          <div className="search-history-content">
            {searchHistory.map((item, index) => (
              <div key={index} className="search-history-chip">
                {item.searchText}
                <span className="search-date">{item.date}</span>
                <div
                  onClick={() => removeSearchHistory(index)}
                  className="close-btn"
                >
                  <svg
                    version="1.1"
                    id="Layer_1"
                    x="0px"
                    y="0px"
                    width="10"
                    height="10"
                    viewBox="0 0 122.878 122.88"
                    enable-background="new 0 0 122.878 122.88"
                  >
                    <g>
                      <path d="M1.426,8.313c-1.901-1.901-1.901-4.984,0-6.886c1.901-1.902,4.984-1.902,6.886,0l53.127,53.127l53.127-53.127 c1.901-1.902,4.984-1.902,6.887,0c1.901,1.901,1.901,4.985,0,6.886L68.324,61.439l53.128,53.128c1.901,1.901,1.901,4.984,0,6.886 c-1.902,1.902-4.985,1.902-6.887,0L61.438,68.326L8.312,121.453c-1.901,1.902-4.984,1.902-6.886,0 c-1.901-1.901-1.901-4.984,0-6.886l53.127-53.128L1.426,8.313L1.426,8.313z" />
                    </g>
                  </svg>
                </div>
              </div>
            ))}
          </div>
        </div>
      )}
    </div>
  );
}

export default App;
